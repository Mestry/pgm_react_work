import React , { Component  } from 'react';
import InputPreview from '../components/InputPreview';
import { connect } from 'react-redux';
import { setMessage } from '../actions/message';
import OffferImageloader from '../components/loader_display';
import { BrowserRouter as Router ,  Route , Link , Switch } from 'react-router-dom';

import  '../src/App.scss';
import homePage from '../src/pages/home';
import contactPage from '../src/pages/contact';
import aboutPage from '../src/pages/about';
import galleryPage from '../src/pages/gallery';
import bookListPage from '../src/pages/booklist';
import businessPage from '../src/pages/business';
import promisePage from '../src/pages/promise';
import Nav from '../src/module/navigation/Nav';

 class App extends Component
 {


    constructor( props ){
        super(props);
        this.state = {         
          display_user_pop : false         
        };
        this.showUser = this.showUser.bind(this);
        this.hideUser = this.hideUser.bind(this);
      }
    
      showUser(e){   
        this.setState({ display_user_pop :  !this.state.display_user_pop  });    
      }
      hideUser(e){
        this.setState({ display_user_pop :  !this.state.display_user_pop  });
      }

    _onChange = (value) => {
        //this.props.dispatch( setMessage(value))
        this.props.Message(value);
    }

    render(){
        
        let extra_props = {    
            name : 'prashant mestry'
          }

        const { message }  = this.props.Message;  
        console.log('message :', message);
        return  ( 
            
    <div className='container'>            
        <Router> 
            <div>
                <ul className='top_nav'>                    
                    <Link  to="/"><li>Home</li></Link>
                    <Link to="/about"><li>About</li></Link>
                    <Link to="/contact"><li>Contact</li></Link>
                    <Link to="/booklist"><li>Book List</li></Link>
                    <Link to="/business"><li>Business</li></Link>
                    <Link to="/promise"><li>Promise</li></Link>
                    <li className='user_box' onMouseOver={ this.showUser } onMouseOut={ this.hideUser }>User
                    {
                        this.state.display_user_pop ?
                        <div className='user_box_pop' > This is user</div>                    
                        :
                        null
                    }
                    </li>                                      
                </ul>
                <hr/>
                <Switch>                
                    <Route   exact path='/' component={ homePage } /> 
                    <Route path='/contact' component={contactPage} />
                    <Route exact path='/about'  { ...extra_props }  component={ aboutPage } />
                    <Route path='/booklist' component={ bookListPage } />            
                    <Route path='/business' component={ businessPage } />            
                    <Route path='/gallery' component={ galleryPage } />
                    <Route path='/promise' component={ promisePage } />             
                </Switch>
                <div className='footer'>
                    <Nav />                 
                </div>
            </div>
          
       </Router>

            <div>
                <div style={{ 'width' : '90%' , 'margin' : '0 auto' , 'border' : '1px solid red' }}>
                    <div style={{ 'width' : '10%' , 'float' : 'left' , 'border' : '1px solid gray' , 'padding' : '20px' }}><OffferImageloader /></div>
                    <div style={{ 'width' : '10%' , 'float' : 'left' , 'border' : '1px solid gray'  , 'padding' : '20px' }}><OffferImageloader /></div>
                    <div style={{ 'width' : '10%' , 'float' : 'left' , 'border' : '1px solid gray'  , 'padding' : '20px' }}><OffferImageloader /></div>
                    <div style={{ 'width' : '10%' , 'float' : 'left' , 'border' : '1px solid gray'  , 'padding' : '20px' }}><OffferImageloader /></div>
                </div> 

                <p>This is my new react app</p>
                <InputPreview  value={ message } data='prashant' onChange={ this._onChange  } />
            </div>
    </div>            
        )
    }
}


const mapDispatchToProps = ( dispatch ) =>{ 
    
    return {
        Message : ( value ) =>   {  dispatch ( setMessage(value)) }
    }
}



export default connect(null , mapDispatchToProps )(App) ;
