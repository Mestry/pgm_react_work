import React from 'react';

export default class InputPreview extends React.Component 
{

    _onChange = (e) =>{
        console.log('value is ' , e.target.value);
        this.props.onChange(e.target.value);
    }

    render () {
        return (
            <div>
                <input style={ myStyle }  type="text" onChange={ this._onChange.bind(this)  }  value={ this.props.value } />
            </div>
        )
    }
}

let myStyle = {
    'border' : '1px solid red',
    'padding' : '10px',
    'fontSize' : '16px'
}