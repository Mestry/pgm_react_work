import React, { Component } from 'react';
import ContentLoader from 'react-content-loader';

class Myloader extends Component{

    constructor(props){
        super(props);        
    }
  
OffferImageloader() {
    return (
        <ContentLoader height={ 400 } >                
            <rect x="50" y="0" rx="5" ry="5" width="290" height="220" />
            <rect x="100" y="280" rx="3" ry="20" width="200" height="3" />
            <rect x="125" y="260" rx="2" ry="2" width="150" height="3" />                
            <rect x="170" y="300" rx="0" ry="0" width="50" height="3" />                
        </ContentLoader>
    )
}

    render(){
        return(
            <div>
                { this.OffferImageloader()  }
            </div>
        )
    }

}

export default Myloader;