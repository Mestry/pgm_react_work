import { createStore, combineReducers, applyMiddleware } from 'redux'
import messageReducer from '../reducers/message';

import BookReducer from  '../src/reducer/reducer_books'
import  AboutReducer  from '../src/reducer/about_reducer';
import  GalleryReducer  from '../src/reducer/gallery_reducer';

import thunk from 'redux-thunk';

const reducer = combineReducers({
    messageReducer , BookReducer , AboutReducer , GalleryReducer
})

const store = createStore(
 reducer,
 applyMiddleware(thunk)
)
export default store;