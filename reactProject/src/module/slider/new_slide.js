
import React , { Component  } from 'react';

import CarouselSlider from "react-carousel-slider"

class NewSlider extends  Component{


        render() {

            let img_data = [
                {
                    des: "Upto 25% North Indian",
                    imgSrc: "public/test/12.jpg"
                },
                {
                    des: "Upto 50% South Indian",
                    imgSrc: "public/test/11.jpg"
                },
                {
                    des: "Upto 75%  Chinese",
                    imgSrc: "public/test/13.jpg"
                },
                {
                    des: "Upto100% North American",
                    imgSrc: "public/test/14.jpg"
                }
            ];
    
            let manner = {
                autoSliding: {interval: "4s"},
                duration: "2s"
            };

            let iconItemsStyle = {
                padding: "0px",
                background: "transparent",                
                margin:"0 30px",
                height: "50%"
            };

            let circleIcon = {
                borderRadius: "10px"
            }
            let textStyle = {
                width: "60%", top: "120%",fontSize: "10px" , color : '#000'  ,
                background : 'none',                
            }
          
            let btnWrapperStyle = {
                position: "relative",
                borderRadius: "50%",
                height: "30px",
                width: "30px",
                boxShadow: "1px 1px 1px 1px #9E9E9E",
                textAlign: "center",
                background : '#fff'
            }

            let btnStyle = {
                display: "inline-block",
                position: "relative",
                top: "50%",
                transform: "translateY(-50%)",
                fontSize: "16px"
            }
            let rBtnCpnt = (<div style = {btnWrapperStyle} >
                <div style = {btnStyle} className = "material-icons" >{'>'}</div>
            </div>);

            let lBtnCpnt = (<div style = {btnWrapperStyle} >
                <div style = {btnStyle} className = "material-icons" >{'<'}</div>
            </div>);   
            
            let iconsSlides = img_data.map((item, index) => 
                <div key = {index} >
                    <img style = {circleIcon} src = {item.imgSrc} ></img>
                    <p style = { textStyle } >{item.des}</p>
                </div>
            );

            let icons = (<CarouselSlider 
                manner = {manner} 
                sliderBoxStyle = {{height: "250px", width: "100%", background: "transparent" , 'border' : '1px solid red'}} 
                accEle = {{dots: false}}
                slideCpnts = {iconsSlides} 
                itemsStyle = {iconItemsStyle}
                buttonSetting = {{placeOn: 'middle-outside'}}
                rBtnCpnt = {rBtnCpnt} 
                lBtnCpnt = {lBtnCpnt}
            />);
            
            return (<div style = {{position:"relative", margin: "0 auto", width: "80%"}} >
                {icons}
            </div>);
        }



}

export default NewSlider;