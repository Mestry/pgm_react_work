
import React , { Component  } from 'react';
import ReactDOM from 'react-dom';

class PhotoSlider extends  Component{

    constructor( props )
    {
        super( props);       
        this.scrollme = this.scrollme.bind(this);
        this.state= {
            left_margin : 0,
            show_left_arrow : 'none',
            show_right_arrow :  'block',
            total_scroll_width : 0,
            image_div_width : 0,
            display_div_width : 0,
        }
    }

    componentDidMount(){
        let image_width  = ReactDOM.findDOMNode(this.test).firstChild.offsetWidth;
        let total_img = ReactDOM.findDOMNode(this.test).childElementCount;
        let display_width = ReactDOM.findDOMNode(this.test).offsetWidth;

            this.setState({
                image_div_width : image_width ,
                total_scroll_width : image_width * total_img,
                display_div_width : display_width
            })
            
    }
    
scrollme(flag ){
    
    let current_left_margin = this.state.left_margin;
        if( flag === 'left' ){
            
            console.log('find');
                
            if( current_left_margin <  0)
            {
                let left_m = this.state.left_margin + this.state.image_div_width;
                ReactDOM.findDOMNode(this.test).firstChild.style.marginLeft = left_m+'px';
                this.setState((prevState, props) => ({
                    left_margin : left_m
                }));                    
            }
            else    
            {
                this.setState({
                    show_left_arrow : 'none',
                    show_right_arrow : 'block'
                })
            }
        }
        if( flag === 'right')
        {                            
                let show_width = this.state.total_scroll_width -  Math.abs( this.state.left_margin) ;
                if( show_width <= this.state.display_div_width )
                {                    
                    this.setState({
                        show_right_arrow : 'none',
                        show_left_arrow : 'block'                    
                    })
                }
                else
                {                
                    let left_m = this.state.left_margin - this.state.image_div_width;
                    ReactDOM.findDOMNode(this.test).firstChild.style.marginLeft = left_m+'px';
                    
                    this.setState((prevState, props) => ({
                        left_margin : left_m,
                        show_left_arrow : 'block'                    
                    }));                        
                }       
        }     
}


    render(){

        return(

            <section className="card-block">                
                <a className='arrow_left' onClick={this.scrollme.bind(this , 'left')}  style={{ 'display' : `${ this.state.show_left_arrow }`  }}>&lt;</a>
                <a className='arrow_right' onClick={this.scrollme.bind(this , 'right')}  style={{ 'display' : `${ this.state.show_right_arrow }`  }}>&gt;</a>
                <div className='card' ref={ node => this.test = node }  >                              

                  {
                        this.props.items.map(( item , index) => {
                            
                            //let ss =  ( index == 0  ) ? `'margin-left : ${ this.state.left_margin }px'` : null;
                            //let ss = ( index == 0) ?  '`border` : `1px solid red`' : null;

                            return(      
                                    <div key={ index } className="card--content" >                                                     
                                        <img  alt='' src={ `../../../../test/${ item }`  }/>
                                        <div className='img_txt'>two Img</div>
                                    </div>
                            )
                        })
                    }
                      
                </div>
            </section>
            
        )
        
    }


}

export default PhotoSlider;