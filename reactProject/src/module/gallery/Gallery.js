import React, { Component } from 'react';

class GalleryShow extends Component{

    constructor( props ){
        super( props );
        this.state = {
            pictures : [],
        };
    }

    componentWillMount(){
        fetch( 'https://randomuser.me/api?results=10' )
        .then( results => {
            return results.json();
        } )
        .then(  data => {
            let pictures = data.results.map( ( resp , index ) => {
                return(
                    <li key={ index }>
                        <img alt='iimg' src={ resp.picture.medium } />
                    </li>    
                )   
            })
                this.setState( { pictures : pictures } );                
        } )
    }

    render(){
        return(
            <div className = "row">
               { this.state.pictures }
            </div>
        );
    }
}

export default GalleryShow;