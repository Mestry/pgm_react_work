import React, { Component } from 'react';
//import ReactDOM from 'react-dom';
import './Nav.css';

class Nav extends Component {

    constructor( props ){
      super( props );
      this.state = {}
    }

    render() {

    let nav_array = [
      {
        name : 'Profile',
        link : 'profile',
        time : '12pm'        
      },
      {
        name : 'Merchant',
        link : 'merchant',
        time : '1pm'        
      },
      {
        name : 'Gallery',
        link : 'gallery',
        time : '2pm'        
      }            
  ];

      return (
        <div>
          <ul className="nav navbar-nav">
            {
              nav_array.map(( data ,  index ) => {
                return (<li attr-data={ index } className={ index === 1 ? 'active' : '' } key={ index }><a href={ '/'+data.link  }> { data.name } </a></li> );
              })
            }          
          </ul>
        </div>
      );
  }
}

export default Nav;
