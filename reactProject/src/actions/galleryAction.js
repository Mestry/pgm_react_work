
import  axios  from 'axios';

const gallery = ( result ) => ({
    type : 'GALLERY_LIST',
    result 
})

 const getGalleryList = ( data ) => async  dispatch => {

    console.log('inside galleryAction getGalleryList action : ' , data);
    console.log('data type : ' , data.num_type);    

    axios.get( 'https://jsonplaceholder.typicode.com/users' ).then( function( response ) {
        //console.log('api output is ' );
        let api_out = response.data;
        let output = [];
         if( data.num_type === 'even' )
         {        
            output = api_out.filter(( cont ) =>  (cont.id % 2) === 0);             
         }else{
            output = api_out.filter(( cont ) =>  (cont.id % 2) !== 0);             
         }                         
        dispatch(gallery( output ));

    }).then(function(){
        console.log('done with response ');        
    })
    
   

}

export default getGalleryList;