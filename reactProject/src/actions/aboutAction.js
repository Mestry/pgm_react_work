import axios from 'axios';
import _ from 'lodash';


// const callAction = result =>({    
//     type : 'ABOUT_CONTENT',
//     result 
// })

const callInc =( result , response2 ) =>({
    type : 'ABOUT_INC',
    result,
    response2
})

const add_two = v => ( v*v )
    

 export const aboutAction = () => async dispatch => {
     
     axios.get('https://jsonplaceholder.typicode.com/comments').then(function( response ){

        let  result1 =   { title : 'About Page Tile one',  content  :  ' one Here is content of about page', count : 100   } ;    
        let new_num = [ '2' ,   '6' , '7' ,'8' , '3' , '4' , '5'   ]; 
        let  map_array = _.map( new_num  , add_two  );        
        console.log('maap array : ' , map_array);
        let filter_array  = _.filter( new_num  , function( num ) {
                return (num > 4);
        });        
        console.log('filter array : ' , filter_array);

        let response1 = [ '11' , '12'  , '13' ,  '---'  ,  ...map_array ,  '---' , ...filter_array  ];
        dispatch(callInc( result1 , response1 ) );

    }).catch(function (error) {
        console.log(error);
    });

 }

/*
export const aboutAction = () => {
    let  result =   { title : 'About Page Tile one',  content  :  ' one Here is content of about page', count : 100   } ;
    return callInc(result)
}

*/



export default aboutAction;