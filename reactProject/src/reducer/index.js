import { combineReducers  } from 'redux';
import BookReducer from './reducer_books';
import  AboutReducer  from './about_reducer';
import  GalleryReducer  from './gallery_reducer';

const appReducer = combineReducers({
    BookReducer,AboutReducer,GalleryReducer
}); 

  export default appReducer;
  