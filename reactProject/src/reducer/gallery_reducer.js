

let initialState = {    
    gallerylist : [ '111' ,'222' , '333' ]
};

 const  GalleryReducer = ( state = initialState , action) => 
{  
     switch(action.type)
     {
        case 'GALLERY_LIST':             
            return {
                ...state , gallerylist : action.result
            }           
        default :        
        //console.log('default case gallery reducer');
        return state;
        
     }
}

export default GalleryReducer;