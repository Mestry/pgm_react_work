import React from 'react';
import ReactDOM from 'react-dom';

// These two containers are siblings in the DOM

const modalRoot = document.getElementById('modal-root');

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.el = document.createElement('div');
  }

  componentDidMount() 
  {
    modalRoot.appendChild(this.el);
    let modal=document.getElementById("modal");

    window.onclick = (event) => {        
    console.log('inside click event ' , event.target);
            if (event.target === modal) {
                //this.props.onclick(event);
                modal.style.display = 'none';
            }
    }
    /*
    
        if (event.target === modal) {
            console.log('inside taget');
            modal.style.display = "none";
            this.props.onClick(event)            
        }        
    }
    */
  }

  componentWillUnmount(){
        //modalRoot.removeChild(this.el);
  }
  

  

  render() {    
    return ReactDOM.createPortal(
        <div className='modal' id='modal'>
            { this.props.children }
        </div>,      
        this.el,
    );
  }
}

export default Modal;