import React, { Component } from 'react';
import Nav from  './module/navigation/Nav';
import logo from './logo.svg';
import { BrowserRouter as Router ,  Route , Link , Switch } from 'react-router-dom';
import './App.css';
import contactPage from '../src/pages/contact';
import homePage from '../src/pages/home';
import aboutPage from '../src/pages/about';
import galleryPage from '../src/pages/gallery';
import bookListPage from '../src/pages/booklist';
import businessPage from '../src/pages/business';
import promisePage from '../src/pages/promise';

class App extends Component {

  constructor( props ){
    super(props);
    this.state = {         
      display_user_pop : false         
    };
    this.showUser = this.showUser.bind(this);
    this.hideUser = this.hideUser.bind(this);
  }

  showUser(e){   
    this.setState({ display_user_pop :  !this.state.display_user_pop  });    
  }
  hideUser(e){
    this.setState({ display_user_pop :  !this.state.display_user_pop  });
  }

  render() {
  let extra_props = {    
    name : 'prashant mestry'
  }

    return (
      <div className="App container">      
       <Router> 
          <div>
              <ul className='top_nav'>
                  <li className='site_img' ><img alt=''  src={ logo } /></li>
                  <Link  to="/"><li>Home</li></Link>
                  <Link to="/about"><li>About</li></Link>
                  <Link to="/contact"><li>Contact</li></Link>
                  <Link to="/booklist"><li>Book List</li></Link>
                  <Link to="/business"><li>Business</li></Link>
                  <Link to="/promise"><li>Promise</li></Link>
                  <li className='user_box' onMouseOver={ this.showUser } onMouseOut={ this.hideUser }>User
                  {
                    this.state.display_user_pop ?
                      <div className='user_box_pop' > 
                          This is user
                      </div>                    
                    :
                    null
                  }
                  </li>                                      
              </ul>
              <hr/>
              <Switch>                
                <Route   exact path='/' component={ homePage } /> 
                <Route path='/contact' component={contactPage} />
                <Route exact path='/about'  { ...extra_props }  component={ aboutPage } />
                <Route path='/booklist' component={ bookListPage } />            
                <Route path='/business' component={ businessPage } />            
                <Route path='/gallery' component={ galleryPage } />
                <Route path='/promise' component={ promisePage } />             
              </Switch>
              <div className='footer'>
                <Nav />                 
              </div>
          </div>
          
       </Router>
      </div>
    );
  }
}

export default App;
