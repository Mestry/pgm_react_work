import React , {  Component } from 'react';
import Galleryshow from  '../module/gallery/Gallery';
import { connect } from 'react-redux';
import  getGalleryList  from '../actions/galleryAction';
import Lightbox from 'react-image-lightbox';
import Loadable from 'react-loadable';



const  LoadableGalleryList = Loadable({
    loader : () =>   import('../module/gallery/Gallery'),
    loading  : ()  => { 
            return  <div> ........ </div>
        },
        delay : 8000    
});

class Gallery extends Component{

    constructor( props){
        super( props );
        this.state={
            num_type : 'even',
            photoIndex: 0,
            isOpen: false,
        }
        this.setNumtype = this.setNumtype.bind(this);
    }

    setNumtype(type)
    {        
        this.setState({ num_type : type });        
        let act = { type : 'GALLERY_LIST' , 'num_type' : this.state.num_type  };        
        this.props.galleryImagelist( act );
    }

    render(){


        const {  photoIndex , isOpen } = this.state;
        let images = [
            'https://randomuser.me/api/portraits/med/men/38.jpg',
            'https://randomuser.me/api/portraits/med/men/9.jpg',
            'https://randomuser.me/api/portraits/med/women/14.jpg'
        ];

        return(
            <div>
                <h2>My Gallery</h2>
                <div className='gallery_box'>
                <button onClick={ (e)=>this.setState({ isOpen :true }) }>Open LightBox</button>
                <br/><br/>
             
                <LoadableGalleryList />
                <br/><br/>

        {isOpen && (
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.setState({ isOpen: false })}
            onMovePrevRequest={() =>
              this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length,
              })
            }
            onMoveNextRequest={() =>
              this.setState({
                photoIndex: (photoIndex + 1) % images.length,
              })
            }
          />
        )}

        <br/><br/>


                { /*<Galleryshow />   */ }
                <button onClick={ () => this.setNumtype( 'even') }>Load Even Gallery</button>
                <button  onClick={ () => this.setNumtype( 'odd') }>Load Odd Gallery</button>
                <br/><br/>           
                {
                     this.props.galleryList.length > 0   ?
                                       
                        this.props.galleryList.map(( data , index ) => {
                        return(
                            //onClick={() => this.props.history.push(`/${getMetroName(this.props.metroid)}/bdiaries/${title_url}-${blog.blogId}` )}
                                    <li onClick={ (e)=>this.props.history.push(`/user/${ data.name }`)  }    key={index}>{ data.website }
                                        <div>{ data.name }</div>
                                        <div>{ data.email }</div>                 
                                        <div>{ /* data.address.city */ }</div>                                        
                                    </li>
                                );                                
                        })                                                        
                    :
                    <div>Length is { this.props.galleryList.length }</div>
                }                                        
                </div>                
            </div>
        )
    }
}

const mapStateToProps = ( state ) =>{
    console.log('new state ', state);
    return {
        galleryList : state.GalleryReducer.gallerylist        
    }
}

// Method ONE to call prop as 'function'.
function mapDispatchToProps (dispatch)
{    
    console.log('map dispatch to props');
    return{
        galleryImagelist : ( data ) => {                
            console.log('load gallery click');
            let act = { type : 'GALLERY_LIST' , 'num_type' : data.num_type };
            dispatch(getGalleryList(act) );             
        }
    }
}

// Method TWO to call prop as  'constant'.
//  const mapDispatchToProps = ( dispatch ) => ({     
//      galleryImagelist : ( data ) =>  dispatch( getGalleryList({ type : 'GALLERY_LIST'  , 'num_type' : data.num_type  }) ) 
//  })

export default connect( mapStateToProps  , mapDispatchToProps)(Gallery);

