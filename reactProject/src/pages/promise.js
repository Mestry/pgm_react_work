import React , {  Component } from 'react';
import { Throttle , Debounce } from 'react-throttle';

class Promise extends Component{
  
    render(){

        return(
            <div>
                <h2>Promise Link</h2>

            <Throttle time='3000' hendler="onChange">
                <input onChange={ () => console.log("Here is input") } />
            </Throttle>

            <Debounce time='3000' hendler="onChange">
                <input onChange={ () => console.log("Here is input two") } />
            </Debounce>

            </div>
        )
    }

}

export default Promise;

