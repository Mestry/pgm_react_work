import React , { Component  } from 'react';
import Booklist from '../container/Booklist';
import Gallery from 'react-photo-gallery';
import photo_array from '../helper/imageArray';

class Booklist_page extends Component{

    render(){
        
        return(
            <div>
                <h2>Book List</h2>
                <div className='box_border'  >
                <Booklist />

                <Gallery photos={ photo_array }  column='5'  />                

                </div>
            </div>
        )
    }
}

export default Booklist_page;


  