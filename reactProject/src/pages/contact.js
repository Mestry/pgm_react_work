import React , {  Component } from 'react';
import Modal from '../modal/modal';
import Iframe from 'react-iframe';

class Contact extends Component{

     constructor(props){
         super(props);
         this.state ={
             color : 'blue',
             flag   : false,
             showPopup: false,
         }
         this.showPopup = this.showPopup.bind(this);
     }

     componentDidMount(){
        console.log('load ', this.state);
     }

     componentWillMount(){
         console.log('component will unmount evene');
     }

     showPopup(){         
        this.setState({ showPopup : !this.state.showPopup } );
        console.log('showPopup click ', this.state);
     }

     handleClick( ){      
        this.setState(( old )  =>({
          flag : !old.flag ,
          color : this.state.flag ? 'green' : 'red'
        }));   
     }
  
    render(){

        let myStyle = {
            background : this.state.color,
            color : '#fff' , 
            border : `1px solid ${ this.state.color }` , 
            padding : '5px' , 
            borderRadius : '5px',
            outline : 'none'
        }

        let getVideoFrame = ( url ) =>{
        return ( <Iframe url = { url } width = '300px'  height="250px" display="initial" position="relative" allowFullScreen /> )
        }

        return(
        <div>               
            <h2>Contact</h2>                     
                { getVideoFrame('http://www.youtube.com/embed/xDMP3i36naA')  }              
        <br/> 
            {
                this.state.showPopup ?  
            
                <Modal onClick={this.showPopup} >
                    <div style={{ 'width' : '200px' , 'border' : '1px solid red' }}>
                    This is content of model
                    </div>
                </Modal>
                    :
                    null
            } 

            <button style={ myStyle }  onClick={ (this).handleClick.bind(this)  }   color='yellow'  >Button &nbsp;
            {  this.state.color } click {  this.state.flag ? 'On' : 'Off' }
            </button>
            <br/><br/>  
            <button onClick={ this.showPopup }>Show Pop</button>

        </div>
        )
    }

}

export default Contact;
