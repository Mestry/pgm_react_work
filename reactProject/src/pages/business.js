import React , {  Component } from 'react';
import LoginBox from '../container/LoginBox';
import FacebookLogin from 'react-facebook-login';
import NewSlider  from '../module/slider/new_slide';
import SlickSlider from '../module/slider/slick_slide';


class Business extends Component{

     constructor(props){
         super(props);
         this.state ={            
             number : [  
             <img width='150' alt='' src='public/test/11.jpg' />,
             <img width='150' alt='' src='public/test/12.jpg' />,
             <img width='150' alt='' src='public/test/13.jpg' />,
             <img width='150' alt='' src='public/test/14.jpg' />,
             <img width='150' alt='' src='public/test/15.jpg' />
            ],
            showModel : true    ,
            mySMS : 'unknown'    
         }        
         this.toggleModel = this.toggleModel.bind(this);     
         this.componentClicked = this.componentClicked.bind(this);      
         this.showSms = this.showSms.bind(this);              
     }

    getTop()
    {         
         let dd = this.state.number;
         let new_i =  dd.shift();
         dd.push(new_i);         
         this.setState({
            number : dd
        })               
    }

     getDown(){
        let dd = this.state.number;
         let new_i =  dd.pop();         
         dd.unshift(new_i);
         this.setState({
            number : dd
        })  

    }

    toggleModel(){
        console.log('toggle model');
        this.setState({
            showModel : !this.state.showModel
        })
    }

    componentClicked(){
        console.log('inside component clicked ');
    }

    showSms(res){
        console.log('here is res : ' ,res);
        this.setState({
            mySMS : res
        })
    }
    
    render(){

        const responseFacebook = (response) => {
            console.log(response);
          }

        return(
        <div>               
            <h2>Business</h2>
                    <SlickSlider />
                <br/>
                     <NewSlider />
                <br/>

            <div className='number_list'>
                <button onClick={ this.getTop.bind( this) }>{'<<'}</button>
                {
                   this.state.number.map(( num , index )=>{
                        return(
                            <li key={ index }>{ num }</li>
                        )
                    })
                }
                <button onClick={ this.getDown.bind( this) }>{'>>'}</button>
            </div>
            <div style={{ 'marginTop' : '50px' }}>
                <FacebookLogin 
                    appId = '1541930382549284' 
                    autoLoad = {  true }
                    fields = "name email picture"                    
                    callback = { responseFacebook }
                />                    
            </div>
            {
                this.state.showModel ?
                    <LoginBox
                        showModel = { this.toggleModel }
                        mySms = { this.showSms } >                                
                        <span> Login Please ,   <br/><span style={{ 'color' : '#4C63E4' }}> {  this.state.mySMS  } </span><br/>  </span>
                        <div>New User </div>        
                        <hr/>                                    
                    </LoginBox>
                    :
                    null
            }
        </div>          
        )
    }
}

export default Business;
