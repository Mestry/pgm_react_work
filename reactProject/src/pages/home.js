import ReactDOM from 'react-dom';
import React , { Component  } from 'react';
import metroList from '../helper/metroName';
import MyAutosuggest from '../module/auto_suggest';
import PhotoSlider from '../module/slider/photo_slider';


class Home extends Component{

    constructor(props){
        super(props);
        this.state = {
            metrolist : [],
            showmetro : 'mumbai',
            userLat : '0',
            userLong : '0',
            left_margin : 0,
            show_left_arrow : 'none',
            show_right_arrow :  'block',
            total_scroll_width : 0,
            image_div_width : 0,
            display_div_width : 0,
            photoitem : 'prasha'
        }
        this.changeBg = this.changeBg.bind(this);
        this.success = this.success.bind(this);
        this.scrollLeft = this.scrollLeft.bind(this);
        this.scrollRight = this.scrollRight.bind(this);
        this.scrollme = this.scrollme.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
    }

    componentWillMount(){    
        let metroNameOnly =   metroList.map( (mm) => mm.metroname  );
        this.setState(( )  =>({
            metrolist : metroNameOnly
        }));       
        //console.log('old metro name is ', localStorage.getItem('new_metro_name'));
   }

    changeBg( index , e )
    {              
        let newM =  e.target.getAttribute('data-metro');
        this.setState({
            showmetro : newM
        });
        localStorage.setItem("new_metro_name", newM);      
    }

    success(pos) {
        var crd = pos.coords;      
        let user_l =  crd.latitude;
        let user_lon =  crd.longitude;     
     
        console.log('Your current position is:');
        console.log(`Latitude : ${ crd.latitude }`);
        console.log(`Longitude: ${ crd.longitude }`);
        console.log(`More or less ${crd.accuracy} meters.`);
         
        this.setState({
            userLat : user_l,
            userLong : user_lon              
        });
        
    }

    error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }


componentDidMount()
{

    let options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };                    
    navigator.geolocation.getCurrentPosition(this.success, this.error, options);     
        
 //   window.addEventListener('scroll', this.handleScroll);

    let image_width  = ReactDOM.findDOMNode(this.test).firstChild.offsetWidth;
    let total_img = ReactDOM.findDOMNode(this.test).childElementCount;
    let display_width = ReactDOM.findDOMNode(this.test).offsetWidth;

    console.log('first image width : ' , image_width);
    console.log(' total_img dom : ' , total_img);
    console.log(' display_div_width dom : ' , display_width);
    
    this.setState({
        image_div_width : image_width ,
        total_scroll_width : image_width * total_img,
        display_div_width : display_width
    })

} 

scrollLeft(){
        
        let image_width  = ReactDOM.findDOMNode(this.test1).firstChild.offsetWidth;
        
        const node = ReactDOM.findDOMNode(this);        
        node.getElementsByClassName('out_box')[0].scrollLeft   -= image_width;        

        //console.log(' out box '  , node.getElementsByClassName('out_box')[0].offsetWidth);
        //console.log( ' scroll pos : ' ,node.getElementsByClassName('out_box')[0].scrollLeft , " " ,  image_width);
}

    scrollRight(){        

        let image_width  = ReactDOM.findDOMNode(this.test1).firstChild.offsetWidth;     

        const node = ReactDOM.findDOMNode(this);            
        node.getElementsByClassName('out_box')[0].scrollLeft +=  image_width;

        //console.log(' out box '  , node.getElementsByClassName('out_box')[0].offsetWidth);
        //console.log( ' scroll pos : ' ,node.getElementsByClassName('out_box')[0].scrollLeft , " " ,  image_width);
    }   

handleScroll(){
    console.log('scrollimng');
    //const node = this.ReactDOM.findDOMNode('.card--content');
    //console.log(' left margin of scroll  ' , node);
}

scrollme(flag ){
    
    console.log('before pos : ' , this.state.left_margin);

    let current_left_margin = this.state.left_margin;
    if( flag === 'left' ){
            
            if( current_left_margin <  0)
            {
                //show_left_arrow : 'block'
                this.setState((prevState, props) => ({
                    left_margin : prevState.left_margin + this.state.image_div_width
                }));    
            }
            else    
            {
                this.setState({
                    show_left_arrow : 'none',
                    show_right_arrow : 'block'
                })
            }
    }
    if( flag === 'right')
    {
            
            let show_width = this.state.total_scroll_width -  Math.abs( this.state.left_margin ) ;

            if( show_width <= this.state.display_div_width ){
                this.setState({
                    show_right_arrow : 'none',
                    show_left_arrow : 'block'                    
                })
            }
            else
            {
                this.setState((prevState, props) => ({
                    left_margin : prevState.left_margin - this.state.image_div_width
                }));    
                this.setState({
                    show_left_arrow : 'block'                    

                })
            }       
    }
     
    console.log('after pos : ' , this.state.left_margin);
}

    render(){

        let photo_list = [ '11.jpg' , '12.jpg' ,'13.jpg' , '14.jpg' , '11.jpg' , '12.jpg' ,'13.jpg' , '14.jpg' ];
        return(
                <div>
                    <h2>Home { this.state.total_scroll_width } { this.state.image_div_width } { this.state.left_margin }</h2>             
                    <br/>                    

                <div className='my_slider'  >
                        <a className='lft_arrow' onClick={this.scrollLeft.bind(this)} >&lt;</a>
                        <a className='rgt_arrow' onClick={this.scrollRight.bind(this)} >&gt;</a>
                        <div className='out_box'  ref={ node1 => this.test1 = node1 }>                              
                                <div className="paper"><img  alt='' src="public/test/11.jpg" /><div className='img_txt'>1 Img</div></div>
                                <div className="paper"><img alt='' src="public/test/12.jpg" /><div className='img_txt'>2 Img</div></div>
                                <div className="paper"><img  alt=''src="public/test/13.jpg" /><div className='img_txt'>3 Img</div></div>
                                <div className="paper"><img  alt=''src="public/test/14.jpg" /><div className='img_txt'>4 Img</div></div>                      
                                <div className="paper"><img  alt=''src="public/test/15.jpg" /><div className='img_txt'>5 Img</div></div>                      
                                <div className="paper"><img  alt=''src="public/test/15.jpg" /><div className='img_txt'>6 Img</div></div>
                                <div className="paper"><img  alt=''src="public/test/12.jpg" /><div className='img_txt'>7 Img</div></div>
                                <div className="paper"><img alt='' src="public/test/13.jpg" /><div className='img_txt'>8 Img</div></div>
                                <div className="paper"><img alt='' src="public/test/14.jpg" /><div className='img_txt'>9 Img</div></div>                      
                                <div className="paper"><img  alt=''src="public/test/15.jpg" /><div className='img_txt'>10 Img</div></div>                      
                                <div className="paper"><img alt='' src="public/test/15.jpg" /><div className='img_txt'>11 Img</div></div>
                            </div>
                </div>








            <section className="card-block" style={{ 'display' : 'none'  }}>                
                <a className='arrow_left'  onClick={this.scrollme.bind(this , 'left')}  style={{ 'display' : `${ this.state.show_left_arrow }`  }}>&lt;</a>
                <a className='arrow_right'  onClick={this.scrollme.bind(this , 'right')}  style={{ 'display' : `${ this.state.show_right_arrow }`  }}>&gt;</a>
                <div className='card' ref={ node => this.test = node } onScroll={ this.handleScroll.bind(this) } >                              
                        <div className="card--content" style={{  'marginLeft' : `${ this.state.left_margin }px`  }}><img  alt='' src="public/test/11.jpg" /><div className='img_txt'>One Img</div></div>
                        <div className="card--content"><img alt='' src="public/test/12.jpg" /><div className='img_txt'>two Img</div></div>
                        <div className="card--content"><img  alt=''src="public/test/13.jpg" /><div className='img_txt'>One Img</div></div>
                        <div className="card--content"><img  alt=''src="public/test/14.jpg" /><div className='img_txt'>One Img</div></div>                      
                        <div className="card--content"><img  alt=''src="public/test/15.jpg" /><div className='img_txt'>One Img</div></div>                      
                        <div className="card--content"><img  alt=''src="public/test/15.jpg" /><div className='img_txt'>One Img</div></div>
                        <div className="card--content"><img  alt=''src="public/test/12.jpg" /><div className='img_txt'>two Img</div></div>
                        <div className="card--content"><img alt='' src="public/test/13.jpg" /><div className='img_txt'>One Img</div></div>
                        <div className="card--content"><img alt='' src="public/test/14.jpg" /><div className='img_txt'>One Img</div></div>                      
                        <div className="card--content"><img  alt=''src="public/test/15.jpg" /><div className='img_txt'>One Img</div></div>                      
                        <div className="card--content"><img alt='' src="public/test/15.jpg" /><div className='img_txt'>One Img</div></div>
                </div>
            </section>
    <br/><br/>















<br/>
 { /* <PhotoSlider items={ photo_list } style={{ 'display' : 'none'  }}  /> */ }


                 
                    
                    <h3>Latitude : {  this.state.userLat } , Longitude : {  this.state.userLong }</h3>
                    <br/> 
                    <div className='clr metro_name_list'>
                        <ul>
                            {        
                                this.state.metrolist.map((mlist , index) => {
                                let classNameShow = ( mlist === this.state.showmetro) ? 'show-bg' : '';
                                return (<li data-metro={mlist} onClick={ this.changeBg.bind(this , index) } key={ index } className={classNameShow} >{ mlist }</li>)
                                })
                            }
                        </ul>   
                    </div>      
                    <MyAutosuggest
                        id="type-c"
                        placeholder="Type 'c'"
                        onChange={this.onChange}
                    />
                    <MyAutosuggest
                        id="type-p"
                        placeholder="Type 'p'"
                        onChange={this.onChange}
                    />  
                </div>
        )
    }

}


export default Home;


  