import React , { Component } from 'react';
import GoogleLogin from 'react-google-login';


 class InnerLogin extends Component{

    constructor(props){
        super(props);        
        this.mySMS = this.mySMS.bind(this);
        this.responseGoogle = this.responseGoogle.bind(this);
        
    }


    componentDidMount(){

    }

    mySMS(){
        let data = "Hi prashant, how is going on";
        this.props.mySms(data);
    }

     responseGoogle = (response) => {
        console.log(response);
      }
       

    render(){    
        return (
            <div>
                <div style= { myStyle }>
                <span onClick={ this.props.showModel } >X</span>  
                <hr/>          
                    { this.props.children }
                    <button onClick={ this.mySMS } >Show SMS</button>
                </div>

                <GoogleLogin 
                    clientId = '691148056359-46l26kbautpkrg30l11kec63uttgl40t.apps.googleusercontent.com'
                    buttonText = 'Login Using Google'
                    onSuccess={ this.responseGoogle }
                    onFailure={ this.responseGoogle }
                />


                
            </div>
        );
    }
}

let myStyle = {
        width : '200px',
        border : '1px solid gray',
        padding : '20px',
        textAlign : 'center',
        marginTop  : '100px'
}

export default InnerLogin;
