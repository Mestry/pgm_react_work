import React , { Component } from 'react';
import InnerLogin from '../container/InnerLogin';

 class LoginBox extends Component{

    render(){    
        return (
            <InnerLogin
                children = { this.props.children }  
                showModel ={ this.props.showModel }                 
                mySms = { this.props.mySms }
            >
            </InnerLogin>
        );
    }
}

export default LoginBox;
