import React , { Component } from 'react';
import { connect } from 'react-redux';

 class Booklist extends Component{

     renderList() {
        return  this.props.books.map(( book , index ) => {
            return  (  <li key={ index } >{  book.title  }</li>  );     
        });
    }

    render(){    
        return (
            <div className='booklist_box'>                
                {  this.renderList() }                       
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
    books : state.BookReducer
    }
}

export default connect( mapStateToProps)(Booklist);
