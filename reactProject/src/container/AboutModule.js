import React , { Component } from 'react';
import { connect } from 'react-redux';
import  aboutAction from '../actions/aboutAction';
import queryString from 'query-string';
import  NotificationSystem  from  'react-notification-system';
//import { notificationStyle } from '../helper/util';
//import _ from 'lodash/find';

 class AboutPage extends Component{    
    //_notificationSystem: null;
    
    

    componentWillReceiveProps(){
        console.log(" **  component will receive props");
    }


    componentDidMount(){
        console.log(" ** component Did mount");
        let queryParams = queryString.parse(window.location.search);
        console.log('locate : ', queryParams.name  );
        this._notificationSystem = this.refs.notificationSystem;
    }

    componentWillMount( nextProps , nextState){
        console.log(" ** component will Mount : " , nextProps , ' ' ,  nextState );
    }

    shouldComponentUpdate(){
        console.log(" ** should component update");
        return true;
    }

    handleBookmark(e)
    {                                
        this._notificationSystem.addNotification({
            message: 'Bookmark Added',
            level: e.target.getAttribute('msg-type'),
            title: e.target.getAttribute('msg-type'),
            position:   e.target.getAttribute('msg_position') ,
            autoDismiss:3,
            dismissible: 'both',
            action: {
                label : 'Ok',
                callback   : function(){
                    console.log('button click');
                }
            }
        });          
    }

    render(){           
        return (
                <div>                   
                    <div>               
                        <div className='message_link'>
                            <a msg_position='tc' msg-type='success' onClick={(e)=>this.handleBookmark(e)} >Success Notify</a> |
                            <a msg_position='tr' msg-type='error' onClick={(e)=>this.handleBookmark(e)} >Error Notify</a> |
                            <a msg_position='bl' msg-type='warning' onClick={(e)=>this.handleBookmark(e)} >Warning Notify</a> |
                            <a msg_position='br' msg-type='info' onClick={(e)=>this.handleBookmark(e)} >Info Notify</a>
                        </div>
                        <br/>
                        
                        <br/><br/>
                        <button onClick={ this.props.OnNewContent  }>Get Content</button>  
                        <br/><br/>                              
                        ( {  this.props.result.count } )
                        ( {  this.props.result.title } )
                        ( {  this.props.result.content } )
                        ( {  this.props.dataList1 } )
                        {
                            this.props.dataList1.map(( data , index ) => {
                            return( <li key={index}>{ data }</li>) 
                            })
                        }                
                    </div>
                    <NotificationSystem ref="notificationSystem" />   
                </div>            
        );
    }
}

function mapStateToProps(state){
    console.log('state : '+state);
    return {    
        result : state.AboutReducer.new_result,
        dataList1 : state.AboutReducer.dataList
    }
}

 function mapDispatchToProps (dispatch){
     return{
         OnNewContent : () => {                
             const act = { type : 'ABOUT_INC'  };    
             dispatch(aboutAction(act));             
         }
     }
 }
 
export default connect( mapStateToProps , mapDispatchToProps  )(AboutPage);
