module.exports =  [
    {
        "metroid": "1",
        "metroname": "bengaluru",        
        "radius": "2910"        
    },
    {
        "metroid": "2",
        "metroname": "delhi",
        "location": "12.972442,77.580643",
        "radius": "2920"        
    },
    {
        "metroid": "3",
        "metroname": "mumbai",
        "location": "12.972442,77.580643",
        "radius": "2930"
    },
    {
        "metroid": "4",
        "metroname": "kolkata",
        "location": "12.972442,77.580643",
        "radius": "2940"        
    },
    {
        "metroid": "5",
        "metroname": "chennai",
        "location": "12.972442,77.580643",
        "radius": "2950"        
    },

]
