
export function notificationStyle(){
    var defaultColors = {
        success: {
          rgb: '70, 200, 190',
          hex: '#46c8be'
        },
        error: {
          rgb: '236, 61, 61',
          hex: '#ec3d3d'
        }
      };
      var defaultShadowOpacity = '0.9';
      
    return {
        NotificationItem: { // Override the notification item  
          success: {
            border: '2px solid ' + defaultColors.success.hex,
            backgroundColor: '#D8F3F1',
            color: '#4b583a',
            WebkitBoxShadow: '0 0 1px rgba(' + defaultColors.success.rgb + ',' + defaultShadowOpacity + ')',
            MozBoxShadow: '0 0 1px rgba(' + defaultColors.success.rgb + ',' + defaultShadowOpacity + ')',
            boxShadow: '0 0 1px rgba(' + defaultColors.success.rgb + ',' + defaultShadowOpacity + ')'
          },
      
          error: {
            border: '2px solid ' + defaultColors.error.hex,
            backgroundColor: '#f4e9e9',
            color: '#412f2f',
            WebkitBoxShadow: '0 0 1px rgba(' + defaultColors.error.rgb + ',' + defaultShadowOpacity + ')',
            MozBoxShadow: '0 0 1px rgba(' + defaultColors.error.rgb + ',' + defaultShadowOpacity + ')',
            boxShadow: '0 0 1px rgba(' + defaultColors.error.rgb + ',' + defaultShadowOpacity + ')'
          },
        },
        Dismiss: {
            DefaultStyle: {
              cursor: 'pointer',
              fontFamily: 'Arial',
              fontSize: '17px',
              position: 'absolute',
              top: '4px',
              right: '5px',
              lineHeight: '15px',
              backgroundColor: '#dededf',
              color: '#ffffff',
              borderRadius: '50%',
              width: '14px',
              height: '14px',
              fontWeight: 'bold',
              textAlign: 'center'
            },
        
            success: {
              color: '#f0f5ea',
              backgroundColor: '#62d0c7'
            },
        
            error: {
              color: '#f4e9e9',
              backgroundColor: '#e4bebe'
            }
          }        
      }
}
  