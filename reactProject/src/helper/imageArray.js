let PHOTO_SET = [
    {
      src: 'https://asset.burrp.com/epi/5/dc/3f/15_53fcd41ead1186c4c5cb5289e25cdcdc.jpg?width=240&height=240&v=1',
      width: 2,
      height: 1
    },
    {
      src: 'https://source.unsplash.com/2ShvY8Lf6l0/800x599',
      width: 1,
      height: 1
    },
    {
        src: 'https://asset.burrp.com/epi/cover-images/300x300/northindian.jpg?width=240&height=240&v=1',
        width: 1,
        height: 1
      },
      {
        src: 'https://asset.burrp.com/epi/e/06/26/19242_e26c972d10eeae1087673ac2ab4c4106.jpeg?width=240&height=240&v=1',
        width: 1,
        height: 1
      },
      {
        src: 'https://asset.burrp.com/epi/5/dc/3f/15_53fcd41ead1186c4c5cb5289e25cdcdc.jpg?width=240&height=240&v=1',
        width: 1,
        height: 1
      },
      {
        src: 'https://source.unsplash.com/2ShvY8Lf6l0/800x599',
        width: 1,
        height: 1
      },
      {
          src: 'https://asset.burrp.com/epi/e/06/26/19242_e26c972d10eeae1087673ac2ab4c4106.jpeg?width=240&height=240&v=1',
          width: 1,
          height: 1
        },
        {
          src: 'https://source.unsplash.com/2ShvY8Lf6l0/800x599',
          width: 2,
          height: 2
        }
  ];

  export default PHOTO_SET;
