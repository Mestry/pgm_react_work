var webpack = require('webpack');
var path = require('path');

var parentDir = path.join(__dirname, '../');

var config = {
    mode : 'development',
    entry: [
        path.join(parentDir, './src/index.js')
    ],
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test:/\.(s*)css$/,
                loaders: ["style-loader", "css-loader", "less-loader", "sass-loader"]
            },
            
        ]
    },
    output: {
        path: parentDir + '/dist',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: parentDir,
        historyApiFallback: true
    }
   
}

module.exports =  config;